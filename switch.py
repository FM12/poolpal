import pyb
import utime


class BlueSwitch(object):

    def __init__(self):
        self.switch_val = 0
        self.switch = pyb.Switch()
        self.switch.callback(self.callback)
        self.time = utime.ticks_ms()

    
    def callback(self):
        deltaTime = utime.ticks_diff(utime.ticks_ms(), self.time)
        if (deltaTime > 100):
            self.switch_val = 1
            self.time = utime.ticks_ms()
        return

    def value(self):
        return_val = self.switch_val
        self.switch_val = 0
        return return_val

