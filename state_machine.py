from biorobotics import Ticker
from states import State
from sensor import SensorState
from state_functions import StateFunctions


class StateMachine(object):

    def __init__(self, ticker_freq):
        self.robot_state = State()
        self.sensor_state = SensorState()
        self.state_functions = StateFunctions(self.robot_state, self.sensor_state, ticker_freq)
        self.ticker = Ticker(0, ticker_freq, self.run)
        return

    def run(self):
        self.sensor_state.update()
        self.state_functions.callbacks[self.robot_state.current]()
        return

    def start(self):
        self.ticker.start()
        return

    def stop(self):
        self.ticker.stop()
        return