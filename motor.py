from biorobotics import PWM
from pin_def import Pin_def as PD
from pyb import Pin

class Motor(object):

    def __init__(self, pwm_freq, motor_int = 1):
        self.motor_index = motor_int - 1
        self.direction_pin = Pin(PD.MOTOR_DIR_PIN_LIST[self.motor_index], Pin.OUT)
        self.pwm_pin = Pin(PD.MOTOR_PWM_PIN_LIST[self.motor_index])
        self.pwm = PWM(self.pwm_pin, pwm_freq)
        return

    def write(self, speed):
        #speed between -1 and 1
        if self.motor_index == 0:
            if speed < 0:
                self.direction_pin.off()
            else:
                self.direction_pin.on()
        elif self.motor_index == 1:
            if speed < 0:
                self.direction_pin.on()
            else:
                self.direction_pin.off()
        self.pwm.write(abs(speed))
        return
