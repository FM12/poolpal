# import matplotlib.pyplot as plt
# import numpy as np
# import csv
# from datetime import datetime 
from biorobotics import SerialPC


class Filter(object):

    def __init__(self, index):

        self.index = index
        self.max_value = 0 #for calibration

        self.u1 = 0 #u(k-1)
        self.u2 = 0 #u(k-2)
        self.y1 = 0 #y(k-1)
        self.y2 = 0 #y(k-2)
        self.t1 = 0 #t(k-1)
        self.t2 = 0 #t(k-2)

        # self.pc = SerialPC(5)

        #sampling rate 300 cf = 50
        # self.high_a = [1,-0.620204102886729,0.240408205773458] #coefficients a for highpass filter (matlab)
        # self.high_b = [0.465153077165047,-0.930306154330093,0.465153077165047] #coefficients b for highpass filter (matlab)

        #sampling rate 300 cf = 20
        # self.high_a = [1,-1.70555214554408,0.743655195048866] #coefficients a for highpass filter (matlab)
        # self.high_b = [0.862301835148237,-1.72460367029647,0.862301835148237] #coefficients b for highpass filter (matlab)

        #sampling rate 300 cf = 10
        self.high_a = [1,-1.85214648539594,0.862348626030081] #coefficients a for highpass filter (matlab)
        self.high_b = [0.928623777856504,-1.85724755571301,0.928623777856504] #coefficients b for highpass filter (matlab)

        # #3hz sampling rate 300
        self.low_a = [1,-1.91119706742607,0.914975834801433] #coefficients a for lowpass filter (matlab)
        self.low_b = [0.000944691843840162,0.00188938368768032,0.000944691843840162] #coefficients b for lowpass filter (matlab)

        #6hz sampling rate 300
        # self.low_a = [1,-1.91119706742607,0.914975834801433] #coefficients a for lowpass filter (matlab)
        # self.low_b = [0.000944691843840162,0.00188938368768032,0.000944691843840162] #coefficients b for lowpass filter (matlab)


    def filter_step(self, u):
        # if self.index == 1:
        #     self.pc.set(0, u)

        #high pass
        t = self.high_b[0] * u + self.high_b[1] * self.u1 + self.high_b[2] * self.u2
        t -= self.high_a[1] * self.t1 + self.high_a[2] * self.t2

        # if self.index == 1:
        #     self.pc.set(1, t)

        #rectify
        t = abs(t)

        # if self.index == 1:
        #     self.pc.set(2, t)

        #low pass
        y = self.low_b[0] * t + self.low_b[1] * self.t1 + self.low_b[2] * self.t2
        y -= self.low_a[1] * self.y1 + self.low_a[2] * self.y2

        # if self.index == 1:
        #     self.pc.set(3, y)

        #update values to prepare for next step
        self.y2 = self.y1
        self.y1 = y
        self.u2 = self.u1
        self.u1 = u
        self.t2 = self.t1
        self.t1 = t

        #check with max_value for calibration
        if self.max_value < y:
            self.max_value = y

        #normalize
        y = y/self.max_value

        # if self.index == 1:
        #     self.pc.set(4, y)
        #     self.pc.send()
        #     print(y)

        #enlarge signal
        y = y*2

        #use treshold
        if y < 0.1:
            y=0


        return y
