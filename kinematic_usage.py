try:
    import ulab as np
    from ulab import linalg as la
except ImportError:
    import numpy as np
    import numpy as la
import math

def calc_joint_velocities(q, desired_velocity):
    if desired_velocity[0] == 0 and desired_velocity[1] == 0:
        return np.zeros((2,1))

    if q[0] == 0:
        q[0] == 0.01
    if q[1] == 0:
        q[1] == 0.01

    # left_bound = [-1*math.cos(q[0]), -1*math.sin(q[0])]
    # right_bound = [math.cos(q[0]), math.sin(q[0])]

    # abs_dv = math.sqrt((desired_velocity[0]**2) + (desired_velocity[1]**2))
    # norm_des_vel = np.array([desired_velocity[0]/abs_dv, desired_velocity[1]/abs_dv])

    # if left_bound[0] < norm_des_vel[0] and norm_des_vel[0] < right_bound[0]:
    #     if left_bound[1] < norm_des_vel[1] and norm_des_vel[1] < right_bound[1]:
    #         return np.zeros((2,1))


    hf0 = np.array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]]) #create h-matrix from f to 0
    ee_pos = la.dot(brockett(q),np.array([[0],[0],[1]])) #origin of frame e
    # print("endeffector_position: {} for q: {}".format(ee_pos, q))
    hf0[0,2] = ee_pos[0,0] #assign x value of ee_pos
    hf0[1,2] = ee_pos[1,0] #assign y value of ee_pos
    adj_h0f = adjoint(h_inverse(hf0)) #find adjoint of h-matrix from f to 0
    # print("hf0: {}, adj_hf0: {}".format(hf0, adj_hf0))
    jacobian_new = la.dot(adj_h0f, jacobian(q)) #multiply adjoint with jacobian
    jacobian_new_new = jacobian_new[1:] #drop upper row of new jacobian (go from jacobian' to jacobian'')
    # print(jacobian_new_new)
    # print("Jacobian: {}, J': {}, J'': {}".format(jacobian(q), jacobian_new, jacobian_new_new))
    try:
        jac_inv = np.linalg.inv(jacobian_new_new)
    except ValueError:
        jac_inv = pseudo_inv(jacobian_new_new)
    except np.linalg.LinAlgError:
        jac_inv = pseudo_inv(jacobian_new_new)
    q_dot = la.dot(jac_inv, np.array([[desired_velocity[0]],[desired_velocity[1]]])) #multiply (jacobian'')^-1 with desired velocity
    # print("endeffector_position: {}".format(ee_pos))

    return q_dot #q_dot is vector of desired joint velocities

def pseudo_inv(matrix):
    # transpose = matrix.transpose()
    # st1 = la.dot(transpose, matrix)
    # st2 = la.inv(st1)
    # st3 = la.dot(st2, transpose)
    if matrix[0,0] == 0:
        matrix[0,0] = 0.001456
    if matrix[1,0] == 0:
        matrix[1,0] = 0.002323
    if matrix[0,1] == 0:
        matrix[0,1] = 0.002765
    if matrix[1,1] == 0:
        matrix[1,1] = 0.001543
    # print(matrix)
    return np.linalg.inv(matrix)

def jacobian(q):
    return np.array([[1,1],[0, 0.35*math.cos(q[0])], [0,0.35*math.sin(q[0])]])

def brockett(q):
    he0_qis0 = np.array([[1,0,0],[0,1,0.7],[0,0,1]])
    eT1q1 = brockett_closed_solution(q[0], 0, 0)
    eT2q2 = brockett_closed_solution(q[1], 0, 0.35)
    tmp = la.dot(eT1q1,eT2q2)
    return la.dot(tmp,he0_qis0)

def ee_position(q):
    return la.dot(brockett(q),np.array([[0],[0],[1]]))

def brockett_closed_solution(angle, rx, ry):
    row1 = [math.cos(angle), -math.sin(angle), rx - rx*math.cos(angle) + ry*math.sin(angle)]
    row2 = [math.sin(angle), math.cos(angle), ry - rx*math.sin(angle) - ry*math.cos(angle)]
    row3 = [0,0,1]
    return np.array([row1, row2, row3])

def adjoint(h_matrix):
    adj = np.zeros((3,3))
    adj[0,0] = 1
    adj[1,0] = h_matrix[1,2] #set ry
    adj[2,0] = -1*h_matrix[0,2] #set -rx
    adj[1,1:] = h_matrix[0,:2] #set first row rotation matrix
    adj[2,1:] = h_matrix[1,:2] #set second row rotation matrix
    return adj

def h_inverse(h_matrix):
    inverse = np.zeros((3,3))
    inverse[2,2] = 1
    rot = np.array([h_matrix[0,:2], h_matrix[1,:2]])
    rot_transpose = rot.transpose()
    inverse[0,:2] = rot_transpose[0]
    inverse[1,:2] = rot_transpose[1]
    point = np.zeros((2,1))
    point[0,0] = h_matrix[0,2]
    point[1,0] = h_matrix[1,2]
    point[:] = la.dot(rot_transpose, point)
    point[0,0] = point[0,0]*-1
    point[1,0] = point[1,0]*-1
    inverse[0,2] = point[0,0]
    inverse[1,2] = point[1,0]
    return inverse

def normalize_vector(vector):
    length = math.sqrt(vector[0]**2 + vector[1]**2)
    return np.array([vector[0]/length, vector[1]/length])
