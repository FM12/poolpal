from switch import BlueSwitch
from biorobotics import AnalogIn
from biorobotics import Encoder
from pin_def import Pin_def as PD
from pyb import Pin
from emg_processor import EMG_processor
import math
from biorobotics import SerialPC

class SensorState(object):

    def __init__(self):
        self.blue_switch = BlueSwitch()
        self.blue_switch_val = 0

        self.potmeter = AnalogIn(PD.POTMETER_1)
        self.potmeter_val = 0

        self.direction_pot = AnalogIn(PD.POTMETER_2)
        self.direction_val = 0
        
        self.encoder1 = Encoder(*PD.MOTOR_ENC_PIN_LIST[0])
        # self.encoder_val1 = math.radians(-75) 
        # self.encoder1.set_counter(int(self.encoder_val1 / (2*math.pi) * 8400))
        self.encoder_val1 = 0

        self.encoder2 = Encoder(*PD.MOTOR_ENC_PIN_LIST[1])
        self.encoder_val2 = math.radians(90) * -1
        self.encoder2.set_counter(int(self.encoder_val2 / (2*math.pi) * 8400))
        #self.encoder_val2 = 0

        self.emgs = [AnalogIn(PD.EMG_PIN_LIST[0]),AnalogIn(PD.EMG_PIN_LIST[1]),AnalogIn(PD.EMG_PIN_LIST[2]),AnalogIn(PD.EMG_PIN_LIST[3])]
        self.emg_processor = EMG_processor()
        self.emg_vals = [0,0,0,0]
        self.pc = SerialPC(4)

        self.joy = [AnalogIn(PD.JOYSTICK_X), AnalogIn(PD.JOYSTICK_Y)]
        self.joy_vals = [0,0]

        self.button_safe = Pin(PD.BUTTON_SAFE, Pin.IN, Pin.PULL_UP)
        self.button_safe_val = 1
        self.button_move_emg = Pin(PD.BUTTON_MOVE_EMG, Pin.IN, Pin.PULL_UP)
        self.button_move_emg_val = 1
        self.button_move_joystick = Pin(PD.BUTTON_JOYSTICK, Pin.IN, Pin.PULL_UP)
        self.button_move_joystick_val = 1
        self.button_aim = Pin(PD.BUTTON_AIM, Pin.IN, Pin.PULL_UP)
        self.button_aim_val = 1
        self.button_shoot = Pin(PD.BUTTON_SHOOT, Pin.IN, Pin.PULL_UP)
        self.button_shoot_val = 1

        self.boardbutton_calibrate = Pin(PD.BUTTON_2, Pin.IN, Pin.PULL_UP)
        self.boardbutton_calibrate_val = 1
        self.boardbutton_shoot = Pin(PD.BUTTON_1, Pin.IN, Pin.PULL_UP)
        self.boardbutton_shoot_val = 1

        return

    def update(self):
        self.blue_switch_val = self.blue_switch.value()
        self.potmeter_val = self.potmeter.read()
        self.encoder_val1 = self.encoder1.counter() / 8400 * 2 * math.pi
        self.encoder_val2 = self.encoder2.counter() / 8400 * 2 * math.pi * -1
        self.direction_val = self.direction_pot.read()
        self.boardbutton_shoot_val = self.boardbutton_shoot.value()
        self.boardbutton_calibrate_val = self.boardbutton_calibrate.value()
        self.emg_vals = self.emg_processor.run_filters([signal.read() for signal in self.emgs])
        self.joy_vals = [round(val.read(),1) for val in self.joy]

        self.button_safe_val = self.button_safe.value()
        self.button_move_emg_val =  self.button_move_emg.value()
        self.button_move_joystick_val = self.button_move_joystick.value()
        self.button_aim_val = self.button_aim.value()
        self.button_shoot_val = self.button_shoot.value()

        return