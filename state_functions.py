from kinematic_usage import calc_joint_velocities
import kinematic_usage as ku
from states import State
from motor import Motor
from pid_controller import PID_pf as PID
import math
import ulab as np

class StateFunctions(object):

    def __init__(self, robot_state, sensor_state, ticker_freq):
        self.robot_state = robot_state
        self.sensor_state = sensor_state
        self.callbacks = {
            State.SAFE: self.safe,
            State.MOVE_EMG: self.move_emg,
            State.MOVE_JOYSTICK: self.move_joystick,
            State.AIM: self.aim,
            State.SHOOT: self.shoot,
            State.CALIBRATE: self.calibrate
        }
        self.motor_1 = Motor(16000, 1) 
        self.motor_2 = Motor(16000, 2)

        self.ticker_freq = ticker_freq

        self.controller1 = PID(1,(1/ticker_freq), 3, 0.9, 0.5)
        self.controller2 = PID(2,(1/ticker_freq), 3, 0.9, 0.5)

        self.q_ref_k1 = np.array([math.radians(0), math.radians(90)])

        #variables for aim state
        self.direction_val_old = 0

        #variables for shoot state
        self.shoot_vector = np.zeros((1,2))
        self.retract_vector = np.zeros((1,2))
        self.shoot_timer = 0
        self.retract_timer = 0
        self.shoot_distance = 0.04
        self.retract_distance = 0.03

        #variables for calibration state
        self.last_motor_encoder_vals = np.zeros((10,2))
        self.calibration_state = np.zeros((3)) #[q1 left boundary found, q1 right boundary found, q2 boundary found]
        self.cal_left_bound = 0

        return

    def safe(self):
        # Entry action
        if self.robot_state.is_changed():
            print("Enter Safe")
            self.robot_state.set(self.robot_state.current)
            self.motors_off()
            print("q_ref_k1: {}".format(self.q_ref_k1))
            print("q_measured1: {}\nq_measured2: {}".format(math.degrees(self.sensor_state.encoder_val1),math.degrees(self.sensor_state.encoder_val2)))

        # Main action

        # Exit action
        if self.sensor_state.button_move_emg_val == 0 or self.sensor_state.blue_switch_val == 1:
            self.robot_state.set(State.MOVE_EMG)
            print("Exit Safe to MoveEMG")
        elif self.sensor_state.button_move_joystick_val == 0:
            self.robot_state.set(State.MOVE_JOYSTICK)
            print("Exit Safe to MoveJoystick")
        elif self.sensor_state.button_aim_val == 0:
            self.robot_state.set(State.AIM)
            print("Exit Safe to Aim")
        elif self.sensor_state.boardbutton_calibrate_val == 0:
            self.robot_state.set(State.CALIBRATE)
            print("Exit Safe to Calibrate")
        return

    def move_emg(self):
        # Entry action
        if self.robot_state.is_changed():
            print("Enter MoveEMG")
            self.robot_state.set(self.robot_state.current)
            
        # Main action
        velocity = self.emg_to_velocity(self.sensor_state.emg_vals)
        joint_velocities = calc_joint_velocities(self.q_ref_k1,velocity)
        self.move_motor(joint_velocities)

        # Exit action
        if self.sensor_state.button_safe_val == 0:
            self.motors_off()
            self.robot_state.set(State.SAFE)
            print("Exit MoveEMG to Safe")
        elif self.sensor_state.button_move_joystick_val == 0:
            self.motors_off()
            self.robot_state.set(State.MOVE_JOYSTICK)
            print("Exit MoveEMG to MoveJoystick")
        elif self.sensor_state.button_aim_val == 0 or self.sensor_state.blue_switch_val == 1:
            self.motors_off()
            self.robot_state.set(State.AIM)
            print("Exit MoveEMG to Aim")
        return

    def move_joystick(self):
        # Entry action
        if self.robot_state.is_changed():
            print("Enter MoveJoystick")
            self.robot_state.set(self.robot_state.current)
            
        # Main action
        velocity = self.joystick_to_velocity(self.sensor_state.joy_vals)
        joint_velocities = calc_joint_velocities(self.q_ref_k1,velocity)
        self.move_motor(joint_velocities)

        # Exit action
        if self.sensor_state.button_safe_val == 0:
            self.motors_off()
            self.robot_state.set(State.SAFE)
            print("Exit MoveJoystick to Safe")
        elif self.sensor_state.button_move_emg_val == 0:
            self.motors_off()
            self.robot_state.set(State.MOVE_EMG)
            print("Exit MoveJoystick to MoveEMG")
        elif self.sensor_state.button_aim_val == 0 or self.sensor_state.blue_switch_val == 1:
            self.motors_off()
            self.robot_state.set(State.AIM)
            print("Exit MoveJoystick to Aim")
        return

    def aim(self):
        # Entry action 
        if self.robot_state.is_changed():
            print("Enter Aim")
            self.robot_state.set(self.robot_state.current)

        # Main action
        direction_val_new = int(self.sensor_state.direction_val*360)
        if abs(direction_val_new - self.direction_val_old) > 2:
            print("Direction value: {}".format(direction_val_new))
            self.direction_val_old = direction_val_new

        # Exit action
        if self.sensor_state.button_safe_val == 0 or self.sensor_state.blue_switch_val == 1:
            self.robot_state.set(State.SAFE)
            print("Exit Aim to Safe")
        elif self.sensor_state.button_move_emg_val == 0:
            self.robot_state.set(State.MOVE_EMG)
            print("Exit Aim to MoveEMG")
        elif self.sensor_state.button_move_joystick_val == 0:
            self.robot_state.set(State.MOVE_JOYSTICK)
            print("Exit Aim to MoveJoystick")
        elif self.sensor_state.button_shoot_val == 0 or self.sensor_state.boardbutton_shoot_val == 0:
            self.robot_state.set(State.SHOOT)
            print("Exit Aim to Shoot")
        return

    def shoot(self):
         # Entry action
        if self.robot_state.is_changed():
            print("Enter Shoot")
            self.robot_state.set(self.robot_state.current)
            dir_val = self.sensor_state.direction_val
            if dir_val < 0.5:
                degrees_val = -360*dir_val+180
            else:
                degrees_val = -360*dir_val+540
            self.shoot_vector = ku.normalize_vector(np.array([-0.5*math.sin(math.radians(degrees_val)), 0.5*math.cos(math.radians(degrees_val))]))
            self.retract_vector = np.array([-1*v for v in self.shoot_vector])
            self.retract_timer = self.ticker_freq*self.retract_distance
            self.shoot_timer = self.ticker_freq*self.shoot_distance
            print("Shoot in direction: {} degrees, [x,y]: {}".format(degrees_val, self.shoot_vector))

        # Main action
        print("eepos: {}".format(ku.ee_position(self.q_ref_k1)))
        finished = False
        if self.retract_timer > 0:
            print("retract")
            joint_velocities = calc_joint_velocities(self.q_ref_k1, self.retract_vector)
            # print("jv {}, qrefk1 {}, rv {}".format(joint_velocities, self.q_ref_k1, self.retract_vector))
            self.move_motor(joint_velocities)
            self.retract_timer -= 1
        elif self.shoot_timer > 0:
            print("shoot")
            joint_velocities = calc_joint_velocities(self.q_ref_k1, self.shoot_vector)
            # print("jv {}, qrefk1 {}, sv {}".format(joint_velocities, self.q_ref_k1, self.shoot_vector))
            self.move_motor(joint_velocities)
            self.shoot_timer -= 1
        else:
            print("finished")
            finished = True
            self.motors_off()
        
        # Exit action
        if self.sensor_state.button_safe_val == 0 or finished or self.sensor_state.blue_switch_val == 1:
            self.robot_state.set(State.SAFE)
            print("Exit Shoot to Safe")
        return

    def calibrate(self):
         # Entry action
        if self.robot_state.is_changed():
            print("Enter Calibrate")
            self.robot_state.set(self.robot_state.current)

        # Main action
        self.calibration_matrix_update()
        if self.calibration_state[0] == 0:
            if self.last_motor_encoder_vals[9,0] != self.sensor_state.encoder_val1:
                self.motor_1.write(0.1)
            else:
                self.motors_off()
                self.cal_left_bound = self.encoder_val1
                self.last_motor_encoder_vals = np.zeros((10,2))
                self.calibration_state[0] = 1
                print("q1 left bound found")
        elif self.calibration_state[1] == 0:
            if self.last_motor_encoder_vals[9,0] != self.sensor_state.encoder_val1:
                self.motor_1.write(-0.1)
            else:
                self.motors_off()
                relative_right_bound = self.sensor_state.encoder_val1
                distance = self.cal_left_bound - relative_right_bound
                right_bound = distance / -2
                self.q_ref_k1[0] = right_bound
                self.sensor_state.encoder_val1 = right_bound
                self.sensor_state.encoder1.set_counter(int(right_bound / (2*math.pi) * 8400))
                self.last_motor_encoder_vals = np.zeros((10,2))
                self.calibration_state[1] = 1
                print("q1 calibrated")
        elif self.calibration_state[2] == 0:
            if self.last_motor_encoder_vals[9,1] != self.sensor_state.encoder_val2:
                self.motor_2.write(0.1)
            else:
                self.motors_off()
                arm2_bound = math.radians(166)
                self.q_ref_k1[1] = arm2_bound
                self.sensor_state.encoder_val2 = arm2_bound
                self.sensor_state.encoder2.set_counter(int(arm2_bound / (2*math.pi) * 8400))
                self.calibration_state[2] = 1
                print("q2 calibrated")
        
        # Exit action
        if self.sensor_state.button_safe_val == 0 or self.sensor_state.blue_switch_val == 1 or self.calibration_state[2] == 1:
            self.robot_state.set(State.SAFE)
            print("Exit Calibrate to Safe")
        return



    def motors_off(self):
        self.motor_1.write(0)
        self.motor_2.write(0)

    def emg_to_velocity(self, emg_list):
        vx = emg_list[1] - emg_list[0] * 0.3 #vx = right - left
        vy = emg_list[2] - emg_list[3] * 0.3 #vy = forward - backward
        return np.array([vx, vy])

    def joystick_to_velocity(self, joy_vals):
        vx = (joy_vals[0]-0.5)*-2 * 0.1
        vy = (joy_vals[1]-0.5)*2 * 0.1
        return np.array([vx, vy])

    def move_motor(self, j_v):
        q_ref1 = self.q_ref_k1[0] + (1/self.ticker_freq)*j_v[0][0]
        q_ref2 = self.q_ref_k1[1] + (1/self.ticker_freq)*j_v[1][0]
        
        if q_ref1 < math.radians(-63):
            q_ref1 = math.radians(-63)
        elif q_ref1 > math.radians(63):
            q_ref1 = math.radians(63)

        if q_ref2 < math.radians(-166):
            q_ref2 = math.radians(-166)
        elif q_ref2 > math.radians(166):
            q_ref2 = math.radians(166)

        zero_angle = 0.05
        if q_ref2 < zero_angle and q_ref2 > -zero_angle:
            # wannabe_ee_pos = ku.ee_position([q_ref1, q_ref2])
            # wannabe_arm_length = math.sqrt((wannabe_ee_pos[0][0]**2) + (wannabe_ee_pos[1][0]**2))
            # if wannabe_arm_length > 0.68:
            #     q_ref1 = self.q_ref_k1[0]
            #     q_ref2 = self.q_ref_k1[1]
            q_ref2 = self.q_ref_k1[1]

        self.q_ref_k1[0] = q_ref1
        self.q_ref_k1[1] = q_ref2

        q_measured1 = self.sensor_state.encoder_val1
        q_measured2 = self.sensor_state.encoder_val2

        step_val1 = self.controller1.step(q_ref1, q_measured1)
        step_val2 = self.controller2.step(q_ref2, q_measured2)

        self.motor_1.write(step_val1)   
        self.motor_2.write(step_val2)
        return


    def calibration_matrix_update(self):
        for i in range(9, 0, -1):
            self.last_motor_encoder_vals[i] = self.last_motor_encoder_vals[i-1]
        self.last_motor_encoder_vals[0] = np.array([self.sensor_state.encoder_val1, self.sensor_state.encoder_val2])



    
