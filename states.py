
class State(object):
    SAFE = 0
    MOVE_EMG = 1
    MOVE_JOYSTICK = 2
    AIM = 3
    SHOOT = 4
    CALIBRATE = 5

    def __init__(self):
        self.previous = None
        self.current = self.SAFE
        return


    def set(self, state):
        self.previous = self.current
        self.current = state
        return
    
    def is_changed(self):
        return self.previous is not self.current