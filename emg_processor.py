from filter import Filter
from biorobotics import SerialPC

class EMG_processor(object):

    def __init__(self):
        self.filters = [Filter(0),Filter(1),Filter(2),Filter(3)]
        self.pc = SerialPC(4)

    def run_filters(self, emgs):
        res = []  
        for i, emg in enumerate(emgs):
            val = self.filters[i].filter_step(emg)     
            self.pc.set(i, val)
            res.append(val)
        self.pc.send()
        return res

